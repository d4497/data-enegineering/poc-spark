# README #

Spark envirnoment 

- Spark clúster Standalone con 3 workers.

- Jupyter Notebook

Detalle:

spark: Spark Master.

Port: 8282

Image: bitnami/spark:3.2.0
/ Port: 8181

Image: jupyter/pyspark-notebook:spark-3.2.0
/ Port: 8888

# Clonar el Repo #
    
    git clone https://gitlab.com/fchasco/spark.git


# Levantar el Contenedor #

    cd Spark
    docker build --rm -t lbertolini/spark .
    cd ..
    docker-compose up -d --no-deps


# Configurar el acceso #

    Jupyter Notebook: http://127.0.0.1:8888
    
    
    Spark Master: http://localhost:8181 (No se ejecuta hasta que la session de spark inicie apuntando al puerto 7077)

En docker compose, enviroment, se puede configurar TOKEN y Password (Ref: https://stackoverflow.com/questions/48875436/jupyter-password-and-docker)

Basta con copiar y pegar la url que devuelve consola para comenzar a utilizar Jupyter. 

    docker logs -f docker_jupyter-spark_1
    
    
# Spark Session local

Para visualizar el job 

    http://localhost:4040/jobs/
    
    Hay ejemplos en la carpeta: /home/jovyan/work/notebooks/

# Spark session con nodos esclavos #

    Este ejemplo ejecuta un archivo .py  que hace un conteo de palabras. Spark master hace uso de la configuracion del contenedor (3 workers) y el tercer parametro es la entrada de al funcion word count. Se adjunta un README.md como ejemplo
    
    docker exec -it docker_spark_1 spark-submit --master spark://spark:7077 /usr/local/spark/app/hello-world.py /usr/local/spark/resources/data/README.md
    
    Para ver el detelle del DAG: http://localhost:8181/


### Como aumentar la cantidad de nodos esclavos ##

Se puede aumentar la cantidad de trabajadores de Spark simplemente agregando nuevos servicios basados ​​en la bitnami/spark:latestimagen al docker-compose.ymlarchivo de la siguiente manera:

    spark-worker-n:

        image: bitnami/spark:3.2.0
        
        networks:
            - default_net
            
        environment:
            - SPARK_MODE=worker
            - SPARK_MASTER_URL=spark://spark:7077
            - SPARK_WORKER_MEMORY=1G
            - SPARK_WORKER_CORES=1
            - SPARK_RPC_AUTHENTICATION_ENABLED=no
            - SPARK_RPC_ENCRYPTION_ENABLED=no
            - SPARK_LOCAL_STORAGE_ENCRYPTION_ENABLED=no
            - SPARK_SSL_ENABLED=no
            
        volumes:
            - ../spark/app:/usr/local/spark/app # Spark scripts folder (Must be the same path in airflow and Spark Cluster)
            - ../spark/resources/data:/usr/local/spark/resources/data #Data folder (Must be the same path in airflow and Spark Cluster)


## Para abrir una consola de Spark con Python y Scala ###

Abrir la consola de la maquina "docker_spark_1".
Comandos:
    
    cd bin
    
Para pyspark:
    pyspark
    
Para scala:
    spark-shell

El contendor hace uso de las imagenes oficiales de Spark (bitnami) y Jupyter Notebook (jovyan)

References:

    https://github.com/bitnami/bitnami-docker-spark

    https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-pyspark-notebook


Para utilizar spark con airflow es necesario instalar las dependencias que se encuentan en el archivo docker_airflow.txt, en la imagen custom de airflow.

## Spark-mssql

* spark-mssql-connector: 
    * Doc: https://docs.microsoft.com/en-us/sql/connect/spark/connector?view=sql-server-ver15
    * Download: https://search.maven.org/search?q=spark-mssql-connector
* JBDC driver: 
    * Doc: https://techcommunity.microsoft.com/t5/sql-server-blog/jdbc-driver-9-4-for-sql-server-released/ba-p/2616513#
    * Download: https://github.com/Microsoft/mssql-jdbc/releases/tag/v9.4.0


## Multiple nodes

> java.lang.UnsupportedClassVersionError: com/microsoft/sqlserver/jdbc/SQLServerDriver has been compiled by a more recent version of the Java Runtime (class file version 60.0), this version of the Java Runtime only recognizes class file versions up to 52.0

Fix: El problema era que había varios jars de sql. Habia que dejar el JRE8. 

Link de jar de JDBC para SQL Server: https://docs.microsoft.com/en-us/sql/connect/jdbc/download-microsoft-jdbc-driver-for-sql-server?view=sql-server-ver15
Github: https://github.com/microsoft/mssql-jdbc
Maven: https://mvnrepository.com/artifact/com.microsoft.sqlserver/mssql-jdbc

Miniscript para validar. Es para ejecutar desde dentro de un worker, en pyspark:

```
from pyspark import SparkContext, SparkConf, SQLContext

appName = "PySpark SQL Server - POC Spark JDBC/ Postgree to SQL"
master = "spark://spark:7077"

conf = SparkConf() \
    .setAppName(appName) \
    .setMaster(master)  \
    .set("spark.driver.extraClassPath","/opt/bitnami/spark/jars/mssql-jdbc-9.4.0.   .jar") \
    .set("spark.executor.extraClassPath","/opt/bitnami/spark/jars/mssql-jdbc-9.4.0.jre8.jar")
#     .set("spark.driver.extraClassPath","/home/jovyan/work/jars/mssql-jdbc-9.4.0.jre8.jar")

sc = SparkContext.getOrCreate(conf=conf)
sc.stop()
sc = SparkContext(conf=conf)
sc


# sc.addJar("/opt/bitnami/spark/jars/mssql/mssql-jdbc-9.4.0.jre11.jar")
# sqlContext = SQLContext(sc)
spark = sqlContext.sparkSession

schema_name = 'bant'
table_name = 'd_sucu'
database = 'DWH_DS'
user = "sql_airflow-int"
password  = "brcpxWRH3UavydPB3sNh"

(spark.read.format("jdbc")
       .option("url", f"jdbc:sqlserver://10.97.12.141;databaseName={database}") 
       .option("driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver") 
       .option("dbtable", schema_name + '.' + table_name)
       .option("user", user)
       .option("password", password)
       .load().createOrReplaceTempView("mytable")
       )

dataFrame=spark.sql("Select * from mytable")
dataFrame.head()
```

### Basic tests

```
root@66d654b03284:/opt/bitnami/spark# java -version
  
  openjdk version "1.8.0_312"
  OpenJDK Runtime Environment (build 1.8.0_312-b07)
  OpenJDK 64-Bit Server VM (build 25.312-b07, mixed mode)
```

| Nombre de versión actual    | Reemplaza al nombre anterior    | Otros formatos antiguos   |
|-----------------------------|---------------------------------|---------------------------|
| Java 8 Java 8 Update x      | JRE 8.0 JRE                     | 1.8 1.8.0_0x              |
| Java 7 Java 7 Update x      | JRE 7.0 JRE                     | 1.7 1.7.0_0x              |

### Java versions

| Java SE version   | Major version                    |
|-------------------|----------------------------------|
| 1.0.2             | 45                               | 
| 1.1               | 45 (Not a typo, same version)    |
| 1.2               | 46                               |
| 1.3               | 47                               |
| 1.4               | 48                               |
| 5.0               | 49                               |
| 6                 | 50                               |
| 7                 | 51                               |
| 8                 | 52                               |
| 9                 | 53                               |
| 10                | 54                               |
| 11                | 55                               |
| 12                | 56                               |
| 13                | 57                               |
| 14                | 58                               |
| 15                | 59                               |
| 16                | 60                               |

Thus, the 'major.minor version 52.0' error is possibly because the jar was compiled in JDK 1.8, but you are trying to run it using a JDK 1.7 environment. The reported number is the required number, not the number you are using. To solve this, it's always better to have the JDK and JRE pointed to the same version.

### Diccionario JAVA

* JRE - Java runtime enviroment
    * Los usuarios de equipos que ejecutan applets y aplicaciones que utilicen la tecnología Java
    * Es un entorno necesario para ejecutar applets y aplicaciones escritas con el lenguaje de programación Java
* JSE - Java Standard edition 
    * Los programadores de software que programen applets y aplicaciones que utilicen la tecnología Java
    * Un kit de desarrollo de software que se utiliza para escribir applets y aplicaciones con el lenguaje de programación Java


* JRE
    * Es la implementación de la Máquina virtual de Java* que realmente ejecuta los programas de Java.
    * El Entorno de ejecución de Java es un complemento necesario para ejecutar programas de Java.  
    * El JRE es más pequeño que el JDK, por lo que necesita menos espacio en el disco.  
    * Incluye el JVM, bibliotecas principales y otros componentes adicionales para ejecutar aplicaciones y applets escritos en Java.    
* JDK - Java Development Kit
    * Se trata de un paquete de software que puede utilizar para desarrollar aplicaciones basadas en Java.
    * Java Development Kit es necesario para desarrollar aplicaciones de Java.
    * El JDK necesita más espacio en el disco porque contiene el JRE junto con varias herramientas de desarrollo.
    * Incluye el JRE, conjunto de clases de API, compilador Java, Webstart y archivos adicionales necesarios para escribir applets y aplicaciones de Java.

**Ref**: https://www.java.com/es/download/help/techinfo.html#:~:text=%C2%BFCu%C3%A1l%20es%20la%20diferencia%20entre%20el%20JRE%20y%20JDK%3F&text=Es%20la%20implementaci%C3%B3n%20de%20la,desarrollar%20aplicaciones%20basadas%20en%20Java.


### Java versions in hadoop, spark and jupyter notebook

* The official images from bitnami are not supporting JDK 11. Their support by this time is in JDK 8 (2022, february). 
    * https://hub.docker.com/layers/bitnami/spark/3.2.0-debian-10-r89/images/sha256-cb19b1bdebc0bc9dc20ea13f2109763be6a73b357b144a01efd94902540f6d27?context=explore
    * https://github.com/bitnami/bitnami-docker-spark/issues/40
* The official image from jupyter notebook, are build with JAVA 11
    * https://hub.docker.com/r/jupyter/pyspark-notebook/tags?page=1&name=spark
    * This image won't work.
    * Another notice, Jupyter is going to be deprecated. Recomendation is to change for jupyterlab (Move to jupyter server, and jupyterlab as frontend)
    * https://github.com/jupyter/docker-stacks/issues/1217

Both of them have multiple supports for spark.
